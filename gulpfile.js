
const { spawn } = require('child_process');
const gulp = require('gulp');
const args = require('optimist').argv;
const gulp_util = require('gulp-util');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const less = require('gulp-less');
const clean_css = require('gulp-clean-css');
const clean = require('gulp-clean');
const gulp_if = require('gulp-if');
const rev = require('gulp-rev');
const source_maps = require('gulp-sourcemaps');
const run_sequence = require('run-sequence');
const image = require('gulp-imagemin');


const env = args.env || 'dev';  // dev/prod
const config = require('./config')(env, process.cwd());
const is_production = env === 'prod';
const is_development = env !== 'prod';
const dist_path = config.dist.path;
const dist_js_path = config.dist.paths.js;
const dist_css_path = config.dist.paths.css;
const dist_image_path = config.dist.paths.images;
const dist_font_path = config.dist.paths.fonts;
const manifest = config.dist.manifest;
const components_min_name = config.dist.components;
const app_min_name = config.dist.app;
let server = null;


// Clean --------------------------------------------
gulp.task('clean-js', ()=> gulp.src(dist_js_path, {read: false}).pipe(clean({force: true})));
gulp.task('clean-css', () => gulp.src(dist_css_path, {read: false}).pipe(clean({force: true})));
gulp.task('clean-image', () => gulp.src(dist_image_path, {read: false}).pipe(clean({force: true})));
gulp.task('clean-font', () => gulp.src(dist_font_path, {read: false}).pipe(clean({force: true})));

// Compile ------------------------------------------
gulp.task('compile-components-min-js', () => {
    return gulp.src([
            'public/components/jquery/dist/jquery.min.js',
            'public/components/bootstrap/dist/js/bootstrap.min.js',
            'public/components/bootstrap/dist/js/bootstrap.bundle.min.js',
        ])
        .pipe(uglify())
        .pipe(concat(components_min_name))
        .pipe(gulp.dest(dist_js_path));
});

gulp.task('compile-js', () => {
    return gulp.src([
        'public/js/*.js',
        'public/js/**/*.js'
        ])
        .pipe(gulp_if(is_development, source_maps.init()))
        .pipe(uglify())
        .pipe(concat(app_min_name))
        .pipe(gulp_if(is_development, source_maps.write()))
        .pipe(gulp.dest(dist_js_path));
});

gulp.task('compile-less', () => {
    return gulp.src('public/less/*.less')
        .pipe(gulp_if(is_development, source_maps.init()))
        .pipe(less())
        .pipe(clean_css())
        .pipe(gulp_if(is_development, source_maps.write()))
        .pipe(gulp.dest(dist_css_path));
});

gulp.task('minify-image', () => {
    return gulp.src('public/images/**')
        .pipe(image())
        .pipe(gulp.dest(dist_image_path));
});

gulp.task('copy-font', () => {
    return gulp.src('public/fonts/**')
        .pipe(gulp.dest(dist_font_path));
});

gulp.task('manifest', () => {
    return gulp.src([
        'dist/**/**',
        '!dist/*',
        '!dist/views/**'
        ])
        .pipe(rev())
        .pipe(gulp.dest(dist_path))
        .pipe(rev.manifest(manifest, {
            base: dist_path,
            merge: true // merge with the existing manifest if one exists
        }))
        .pipe(gulp.dest('dist/assets'));
});


// Web Server ---------------------------------------
gulp.task('web-server', () => {
    if (server) {
        server.kill();
    }

    server = spawn('node', ['server.js']);

    server.stdout.on('data', (data) => {
        console.log(data.toString());
    });
  
    server.stderr.on('data', (data) => {
        console.log(data.toString());
    });
});


// run default task before
gulp.task('watch', ['web-server'], () => {
    gulp.watch([
        'app/**',
        'config/**',
        'lib/**',
        '*.js'
    ], ['web-server']);
    gulp.watch('templates/**', ['web-server']);
    gulp.watch('public/js/**', ['clean-js', 'manifest']);
    gulp.watch('public/less/*.less', ['clean-css', 'manifest']);
});


gulp.task('default', [
    'clean-js', 
    'clean-css', 
    'clean-image', 
    'clean-font'], () => {
    run_sequence(
        'compile-components-min-js',
        'compile-js',
        'compile-less',
        'minify-image',
        'copy-font',
        'manifest'
    );    
});