
const router = require('express').Router();
const main = require('./main');

module.exports = router;

router.get('/', main);
