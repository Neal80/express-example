
const Helper = require('../../utils').Helper;
const render = global.dots;

module.exports = (req, res) => {
    const model = {
        helper: Helper,
        attr_test: 'From Model',
    };

    res.send(render.nsd.main(model));
};
