
const router = require('express').Router();
const home = require('./home');
const nsd = require('./nsd');

module.exports = router;


router.get('/', home);
router.use('/nsd', nsd);


