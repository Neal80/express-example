
module.exports = (err, req, res, next) => {
    if (err) {
        console.error(err);

        res.status(500).send('Something broke!');
        return;
    }
            
    next();
};