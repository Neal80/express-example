
const path = require('path');
const express = require('express');
const timeout = require('connect-timeout')
const response_time = require('response-time');
const favicon = require('serve-favicon');
const body_parser = require('body-parser');
const cookie_session = require('cookie-session');
const csrf = require('csurf');
const logger = require('../lib').log;
const dot_ext = require('../lib').dot_ext;
const {error_handler} = require('./middlewares');
const routes = require('./routes');


module.exports = (config) => {
    const app = express();

    app.set('views', path.join(process.cwd(), 'templates'));
    app.set('view engine', 'dot');
    
    app.use(timeout('10s'));
    app.use(response_time(logger.http_log));

    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
    app.use(express.static(path.join(process.cwd(), 'public')));
    app.use(express.static(path.join(process.cwd(), 'dist')));
    app.use(body_parser());
    
    app.use(cookie_session({
        name: 'session',
        keys: ['secret_keys'],
        maxAge: 24 * 60 * 60 * 1000 // 24 hours
    }));

    //app.use(csrf({ cookie: true }));

    app.use('/', routes);

    // error handler
    app.use(error_handler);

    return app;
};