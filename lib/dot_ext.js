
/*
    jst: 会被编译成js文件
    dot: 会被编译成函数。使用: dot.filename(data)
    def: 不会被编译出来，用户{{#def.some }}的引用
    .dot.def: 既会成为一个函数 dot.some ，又能被其他模板使用 {{#def.some }} 引用进来。

    The parameter of partial is just a simple word replacement. 
    So you should keep it unique in the partial content.
    {{##def.block1:param:
        <div>{{=param.a}}</div>
        block1 中不能出现param这个词
    #}}

    无法直接传递参数到def.file里，但是可以在file里面第一个 {{##def.block1:param: #}}，
    先引用文件，再渲染block时，将参数传入
*/

/*
    1.支持layout
    2.partial参数
        无法直接传递参数到def.file里，但是可以在file里面第一个 {{##def.block1:param: #}}，
		先引用文件，再渲染block时，将参数传入
		window.render.param_partial.partial({})
	3.Server端渲染
		template引用partial中的def.block, 保持dot不变：
	  		{{#def.partial_file}}            // 引入文件
			{{#def.block:it.partial || ''}} // 使用block

		渲染子文件夹中页面
			res.send(render.subFolder.filename());

	3.客户端渲染
	  客户端引用partial里面的def.block: window.render.partial.block({})
    5.html输出
    6.html过滤
*/

const path = require('path');
const fs = require("fs");
const doT = module.exports = require("dot");


doT.__express = function(options) {
	/* not implement */
    return function(file_path, o, callback) {
		console.log('in view engine');
		
        callback(null, 'Something broke in engine.');
    };
};

doT.process = function(options) {
	//path, destination, global, rendermodule, templateSettings
	return new InstallDots(options).compileAll();
};


// Copy from dot
function InstallDots(o) {
	this.o              = o;
	this.__layout_exp   = o.layoutExp    || /\.ly(\.def|\.dot|\.jst|\.html)?$/,
	this.__partial_exp  = o.partialExp   || /\.def(\.dot|\.jst|\.html)?$/,
	this.__fn_exp       = o.fnExp        || /\.dot(\.def|\.jst|\.html)?$/,
	this.__file_exp     = o.fileExp      || /\.jst(\.dot|\.def|\.html)?$/,
	this.__path 		= path.join(o.path || "./");
	if (this.__path[this.__path.length-1] !== path.sep) this.__path += path.sep;
	this.__destination	= path.join(o.destination || this.__path);
	if (this.__destination[this.__destination.length-1] !== path.sep) this.__destination += path.sep;
	this.__global		= o.global || "window.render";
	this.__rendermodule	= o.rendermodule || {};
	this.__settings 	= o.templateSettings ? copy(o.templateSettings, copy(doT.templateSettings)) : undefined;
    this.__includes		= {};
	this.__layouts      = {};
	this.__templates    = {};
}

InstallDots.prototype.compileToFile = function(filePath, template, def) {
	def = def || {};
	var moduleName
		, modulePaths = filePath.replace(this.__destination, '').split(path.sep)
		, registerCode = []
		, stepPath = `${this.__global}`
        , defs = copy(this.__includes, copy(def))
		, settings = this.__settings || doT.templateSettings
		, compileoptions = copy(settings)
		, defaultcompiled = doT.template(template, settings, defs)
		, exports = []
		, compiled = ""
		, fn
		, dirname;

	// ./some/page.jst module name is some_page
	registerCode.push(`${this.__global}=${this.__global} || {};`);
	for (let i = 0; i < modulePaths.length - 1; i++) {
		const item = modulePaths[i];
		stepPath = `${stepPath}['${item}']`;
		registerCode.push(`${stepPath}=${stepPath} || {};`);
	}
	moduleName = modulePaths[modulePaths.length -1];
	moduleName = moduleName.substring(0, moduleName.lastIndexOf("."));
	stepPath = `${stepPath}['${moduleName}']`;
	registerCode.push(`${stepPath}=itself;`);
	
	for (var property in defs) {
		if (defs[property] !== def[property] && defs[property] !== this.__includes[property]) {
			fn = undefined;
			if (typeof defs[property] === 'string') {
				fn = doT.template(defs[property], settings, defs);
			} else if (typeof defs[property] === 'function') {
				fn = defs[property];
			} else if (defs[property].arg) {
				compileoptions.varname = defs[property].arg;
				fn = doT.template(defs[property].text, compileoptions, defs);
			}
			if (fn) {
				compiled += fn.toString().replace('anonymous', property);
				exports.push(property);
			}
		}
	}
	compiled += defaultcompiled.toString().replace('anonymous', moduleName);

	dirname = path.dirname(filePath);
	if (!fs.existsSync(dirname)) mkdirPath(dirname);

	fs.writeFileSync(filePath, "(function(){" + compiled
		+ "var itself=" + moduleName + ", _encodeHTML=(" + doT.encodeHTMLSource.toString() + "(" + (settings.doNotSkipEncoded || '') + "));"
		+ addexports(exports)
		+ "if(typeof module!=='undefined' && module.exports) module.exports=itself;else if(typeof define==='function')define(function(){return itself;});else {"
		+ registerCode.join('') +"}}());");
};

function buildModulePaths(destinationPath, relativePath) {
	const paths = relativePath.replace(destinationPath, '').split(path.sep);
	let moduleName = paths[paths.length - 1];
	moduleName = moduleName.substring(0, moduleName.lastIndexOf("."));
	paths[paths.length - 1] = moduleName;

	return paths;
}

function addexports(exports) {
	var ret = '';
	for (var i=0; i< exports.length; i++) {
		ret += "itself." + exports[i]+ "=" + exports[i]+";";
	}
	return ret;
}

function copy(o, to) {
	to = to || {};
	for (var property in o) {
		to[property] = o[property];
	}
	return to;
}

function readdata(path) {
	var data = fs.readFileSync(path);
	if (data) return data.toString();
	console.error("problems with " + path);
}

function mkdirPath(dirname) {
	const paths = dirname.split(path.sep);

	let current_dir = '';
	paths.forEach((d) => {
		current_dir = path.join(current_dir, d !== path.sep ? `${d}/` : d); // window/linux
		if (!fs.existsSync(current_dir)) fs.mkdirSync(current_dir);
	});
}

InstallDots.prototype.compilePath = function(data) {
	if (data) {
		return doT.template(data, this.__settings || doT.templateSettings, copy(this.__includes));
	}
};

InstallDots.prototype.resolveLayout = function(data) {
	const re = ((this.o && this.o.templateSettings) ? this.__settings : doT.templateSettings).use;
	const layout_def = 'layout.';
	const body_def = 'body';
	let body_str = (typeof data === "string") ? data : data.toString();
	
	let def = re.exec(body_str);
	if (def === null) return data;

	let define = def[0];
	let code = def[1];
	let index = def.index;

	if (code.indexOf(layout_def) !== 0) return data;

	const name = path.join(code.substring(layout_def.length)).split(path.sep).join('_');
	const layout_data = this.__layouts[name];

	if (layout_data) {
		def = re.exec(layout_data);
		if (def === null) return data;

		body_str = body_str.slice(0, index) + body_str.slice(index + define.length);
		
		define = def[0];
		code = def[1];
		index = def.index;

		if (code.indexOf(body_def) !== 0) return data;

		return layout_data.slice(0, index) + body_str + layout_data.slice(index + define.length);
	}

	return data;
}

InstallDots.prototype.resolveTemplates = function() {
	/*
		templates = {
			home_dot.dot.html: {
				"name": "home_dot.dot.html",
				"path": "F:\\Projects\\work\\site\\templates\\home_dot.dot.html"
			},
			layouts\layout_login.ly.html: {
				"name": "layout_login.ly.html",
				"path": "F:\\Projects\\work\\site\\templates\\layouts\\layout_login.ly.html"
			}
		};
	*/
	const self = this;
	const basePath = path.join(this.__path);
	let key, absolutePath, stats;

	const resolve = function(dir) {
		fs.readdirSync(dir).forEach((filename) => {
			absolutePath = path.join(dir, filename);

			stats = fs.statSync(absolutePath);
			if (stats.isDirectory()) {
				resolve(absolutePath);
			} else {
				key = absolutePath.replace(basePath, '');
				key = key.substring(0, key.indexOf('.'));

				self.__templates[key] = {
					name: filename,
					path: absolutePath,
				};
			}
		});
	};

	resolve(basePath);
};

InstallDots.prototype.clearAll = function(dir) {
	let absolutePath, stats;

	const clear = function(dir) {
		if (!fs.existsSync(dir)) return;

		fs.readdirSync(dir).forEach((filename) => {
			absolutePath = path.join(dir, filename);

			stats = fs.statSync(absolutePath);
			if (stats.isDirectory()) {
				clear(absolutePath);
			} else {
				fs.unlinkSync(absolutePath);
			}
		});
	};

	clear(dir);
}

InstallDots.prototype.compileAll = function() {
	if (doT.log) console.info("Clear all compiled templates.");
	this.clearAll(this.__destination);
	
	if (doT.log) console.info("Compiling all doT templates...");

	this.resolveTemplates();

	const buildTemplates = (fn) => {
		Object.keys(this.__templates).forEach((k) => 
			fn(this.__templates[k].name, k, this.__templates[k].path));
	};

	const registerModule = (relativePath, to, data) => {
		const namePaths = relativePath.split(path.sep);

		let refs = to;
		namePaths.forEach((x, i) => {
			refs[x] = (i < namePaths.length - 1) ? refs[x] || {} : refs[x] = data;
			refs = refs[x];
		});
	};

	// build layouts
	buildTemplates((name, relativePath, absolutePath) => {
		if (this.__layout_exp.test(name)) {
			if (doT.log) console.info("Loaded layout " + relativePath);
			this.__layouts[relativePath.split(path.sep).join('_')] = readdata(absolutePath);
		}
	});

	/*
		build def partials
		{{#def.subFolder.block}}
	*/
	buildTemplates((name, relativePath, absolutePath) => {
		if (this.__partial_exp.test(name)) {
			if (doT.log) console.info("Loaded def " + relativePath);
			const data = this.resolveLayout(readdata(absolutePath));
			registerModule(relativePath, this.__includes, data);
			this.compileToFile(path.join(this.__destination, relativePath + '.js'), data);
		}
	});

	// build jst and dot
	buildTemplates((name, relativePath, absolutePath) => {
		if (this.__fn_exp.test(name)) {
			if (doT.log) console.info("Compiling " + relativePath + " to function");
			const data = this.resolveLayout(readdata(absolutePath));

			registerModule(relativePath, this.__rendermodule, this.compilePath(data));
		}
		if (this.__file_exp.test(name)) {
			if (doT.log) console.info("Compiling " + relativePath + " to file");
			
			const data = this.resolveLayout(readdata(absolutePath));
			this.compileToFile(path.join(this.__destination, relativePath + '.js'), data);

			// for server render
			registerModule(relativePath, this.__rendermodule, this.compilePath(data));
		}
	});

	return this.__rendermodule;
};