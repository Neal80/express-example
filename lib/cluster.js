
const cluster = require('cluster');
cluster.schedulingPolicy = cluster.SCHED_RR;

const os = require('os');
let cpus = os.cpus().length;
let terminating = false;


module.exports = (fn, worker_processes)  => {
    cpus = worker_processes || cpus;

    if (cluster.isMaster) {
        console.info(`Master(${process.pid}) is running.`);

        for(let i = 0; i < cpus; i++) cluster.fork();

        cluster.on('listening', (worker, address) => {
            console.info(`Worker(${worker.process.pid}) listening on port ${address.port}.`);
        });
        cluster.on('exit', (wokr, code, signal) => {
            console.warn(`Worker(${worker.process.pid}) exit(${signal || code}), fork new.`);
            cluster.fork();
        });

        process.on('SIGTERM', terminate);
        process.on('SIGQUIT', terminate);
    } else {
        fn();
    }
};

const terminate = () => {
    if (terminating) return;
    terminating = true;

    console.info('Terminate, kill workers.');
    
    cluster.removeAllListeners('exit');
    Object.keys(cluster.workers).forEach((id) => cluster.workers[id].kill('SIGTERM'));
}
