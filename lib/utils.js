
const path = require('path');
const fs = require('fs');
const child_process = require('child_process');

function clone(obj) {
    var p, s;

    if (obj === null) return obj;
    if (typeof obj !== 'object') return obj;

    if (obj.constructor === Array) {
        s = [];
        for (var i = 0; i < obj.length; i++) s.push(obj[i]);
    } else {
        s = {};
        for (p in obj) if (Object.prototype.hasOwnProperty.call(obj, p)) s[p] = clone(obj[p]);
    }

    return s;
};

function merge (original, target) {
    if (target === void 0 || target === null) return clone(original);
    if (original === void 0 || original === null) return target;

    for (var p in target){
        if (Object.prototype.hasOwnProperty.call(target, p)) {
            if (target[p] instanceof Array) {
                if (original[p] !== void 0 && original[p] !== null) target[p] = clone(original[p]);
            } else if (typeof(target[p]) === 'object') {
                target[p] = merge(original[p], target[p]);
            } else {
                if (typeof(original[p]) !== 'undefined') target[p] = clone(original[p]);
            }
        }
    }

    return target;
};

function mkdir(dirname) {
    const paths = dirname.split(path.sep);
    
	let current_dir = '';
	paths.forEach((d) => {
		current_dir = path.join(current_dir, d !== path.sep ? `${d}/` : d); // window/linux
		if (!fs.existsSync(current_dir)) fs.mkdirSync(current_dir);
	});
}

function get_last_commit_hash() {
    try {
        const stdout = child_process.execSync(`cd ${path.join(process.cwd())} && git rev-parse --short HEAD`);
        const hash_code_str = stdout.toString();
        return (hash_code_str && hash_code_str !== '') ? hash_code_str.trim() : '';
    } catch(e) {
        return '';
    }
}


module.exports = {
    clone,
    merge,
    mkdir,
    get_last_commit_hash,
};