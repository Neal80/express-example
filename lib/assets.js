
const fs = require('fs');
const path = require('path');

exports.get_manifest = (config) => {
    if (global.manifest) return global.manifest;

    const mf_path = path.join(config.absolute_path, config.dist.path, config.dist.manifest);

    if (fs.existsSync(mf_path)) {
        return require(mf_path);
    } 

    throw new Error('Can not found manifest file.');
};

exports.get_file_path = (file_path) => {
    const manifest = this.get_manifest();
    
    const path_and_file = manifest[file_path];
    if (path_and_file) {
        return path_and_file;
    }

    throw new Error(`Can not found path: ${file_path}`);
};