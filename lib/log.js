
const bunyan = require('bunyan');
const RotatingFileStream = require('bunyan-rotating-file-stream');
const by_format = require('bunyan-format');
const utils = require('./utils');
const by_format_out = by_format({ outputMode: 'long' });


const http_log = (req, res, time) => {
    /*
        for response-time module
        use to http log
        [PID][time][method(statusCode)] url
    */
   console.info(`[${process.pid}][${parseInt(time)}ms][${req.method}][${res.statusCode}] ${req.originalUrl}`);
};


const suppress_console = (logger) => {

    ['log', 'info','warn','error'].forEach((x) => {
        console[x] = (function(fn_name) {
            return function() {
                logger[fn_name].apply(logger, arguments);
            };
        })(x === 'log' ? 'debug' : x);
    });
    
};

const new_log = (config) => {

    let log_stream = by_format_out;
    if (!config.debug) {
        utils.mkdir(config.log.path);
        log_stream = new RotatingFileStream(config.log.options);
    }

    const logger = bunyan.createLogger({
        name: config.log.name,
        level: config.log.level,
        stream: log_stream
    });

    suppress_console(logger);

    return logger;
};


module.exports = {
    new_log,
    http_log,
};