
const path = require('path');
const dot = require('dot-layout');
const args = require("optimist").argv;
const logger = require('./lib').log;
const assets = require('./lib').assets;


const env = args.env || 'dev';

// init config
const config = global.config = require('./config')(env, process.cwd());

// init node env
process.env.NODE_ENV = config.NODE_ENV;
process.env.APP_PATH = process.cwd();
process.env.PORT = config.port;

const log = global.log = logger.new_log(config);
const dots = global.dots = dot.process(config.dot);
const manifest = global.manifest = assets.get_manifest(config);
const app = require('./app')(config);


console.info(`Last commit hash: ${config.last_commit_hash}`);

process.on('uncaughtException', (err) => {
    console.error('Uncaught Exception: ', err.message, err.stack);
    process.exit(1);
});

// run server/cluster
if (config.debug) {
    app.listen(config.port, () => {
        console.info(`Server listening on port ${config.port}`);        
    });
} else {
    require('./lib').cluster(() => app.listen(config.port), config.worker_processes);
}
