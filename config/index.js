
const path = require('path');
const utils = require('../lib').utils;


exports.ENV_TYPES = ['dev', 'prod'];


module.exports = (env, app_path) => {
    const current_env = env || 'dev';
    
    if (this.ENV_TYPES.findIndex(x => x === current_env) === -1) {
        throw new Error('Argment env is invalid.');
    }

    let default_conf = require('./default');
    let override_conf = require(`./${current_env}`);

    const config = utils.merge(override_conf, default_conf);

    // init some config items on runtime
    config.absolute_path = app_path;
    config.last_commit_hash = utils.get_last_commit_hash();
    config.dot.destination = path.join(app_path, config.dot.destination);
    config.dot.path = path.join(app_path, config.dot.path);

    return config;
};