module.exports = {
    NODE_ENV: 'production', // development, production
    debug: false,
    worker_processes: 2,
};