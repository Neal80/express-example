module.exports = {
    NODE_ENV: 'development', // development, production
    debug: true,
    port: 5000,
    worker_processes: 1,
    version: '1.0.0',   // from package.json.version
    last_commit_hash: '', // set on runtime
    absolute_path: '', // set on runtime

    dist: {
        path: 'dist',
        manifest: 'manifest.json',
        components: 'all.min.js',
        app: 'app.min.js',
        paths: {
            js: 'dist/js',
            css: 'dist/css',
            images: 'dist/images',
            fonts: 'dist/fonts',
        },
    },

    log: {
        name: 'app',
        level: 'debug',
        path: 'logs',
        options: { // RotatingFile
            path: 'logs/main-%Y-%m-%d.log',
            period: '1d',          // daily rotation
            gzip: true             // Compress the archive log files to save space
        },
    },

    dot: {
        global: 'window.render',
        destination: 'dist/views',  // Replace with absolute path on runtime 
        path: 'templates'  // Replace with absolute path on runtime 
    }
};